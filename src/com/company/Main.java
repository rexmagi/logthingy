package com.company;
        import javax.swing.JOptionPane;

        import java.io.*;
        import java.util.*;
        import java.text.*;

public class Main
{
    public static class lumber_data{


        public int selection;
        public int[] inumber=new int[100];
        public String[] iname=new String[100];
        public String[] vname=new String[100];
        public double[] vprice=new double[100];
        public double[] rprice=new double[100];
        public int[] stock=new int[100];
        public int count=-1;
        //******************************************************************************
        public static int start_program(int count,int[]inumber,String[]iname,String[]vname,
                                        double[]vprice,double[]rprice,int[]stock)
        {
            String newLine;
            try
            {
                //define a file variable for Buffered read
                BufferedReader lumber_file = new BufferedReader(new FileReader("inventj.dat"));
                //read lines in file until there are no more lines in the file to read
                while ((newLine = lumber_file.readLine()) != null)
                {
                    //there is a "#" between each data item in each line
                    StringTokenizer delimiter = new StringTokenizer(newLine,"#");
                    count=count+1;
                    inumber[count] = Integer.parseInt(delimiter.nextToken());
                    iname[count] =delimiter.nextToken();
                    vname[count] =delimiter.nextToken();
                    vprice[count] = Double.parseDouble(delimiter.nextToken());
                    rprice[count] = Double.parseDouble(delimiter.nextToken());
                    stock[count] = Integer.parseInt(delimiter.nextToken());
                }//while loop
                lumber_file.close();
            }//end try
            catch (IOException error)
            {
                //there was an error on the file writing
                System.out.println("Error on file read " + error);
            }//error on read
            return count;
        }//end start_program

        //******************************************************************************
        public static void end_program(int count,int[]inumber,String[]iname,
                                       String[]vname,double[]vprice,double[]rprice,int[]stock)
        {
            int i;
            //write to the back to the data file
            try
            {
                BufferedWriter lumber_file = new BufferedWriter(new FileWriter("inventj.dat"));
                for (i=0; i<=count; ++i)
                {
                    //put "#" between each data item in the file
                    lumber_file.write(inumber[i]+"#"+iname[i] + "#" + vname[i] + "#" +vprice[i]+ "#"+rprice[i]+ "#" +stock[i]+ "#");
                    //write a new line in the file
                    lumber_file.newLine();
                }//for loop
                lumber_file.close();
            }//end try
            catch (IOException error)
            {
                //there was an error on the write to the file
                System.out.println("Error on file write " + error);
            }//end error
        }//end end_program
        public lumber_data(){
            count=start_program(count,inumber,iname,vname,vprice,rprice,stock);
        }

    }
    public static class lumber_manipulate{
        public static int menu()
        {
            int selection;
            String value;
            String output="ACME LUMBER OUTLET"+"\n"+
                    "=================="+"\n"+
                    "1. Add another item"+"\n"+
                    "2. Report Section"+"\n"+
                    "3. Exit"+"\n"+
                    "Enter your slection > ";

            value =JOptionPane.showInputDialog(null,
                    output,"Input Data",JOptionPane.QUESTION_MESSAGE);
            selection =Integer.parseInt(value);
            return selection;
        }//end menu

        public static void report(int count,int[]inumber,String[]iname,String[]vname,
                                  double[]vprice,double[]rprice,int[]stock)
        {
            int selection,i,search_num;
            String value;
            String output="ACME LUMBER MENU"+"\n"+
                    "=================="+"\n"+
                    "1. Search for an item"+"\n"+
                    "2. Print all items"+"\n"+
                    "3. Print the value of Inventory"+"\n"+
                    "4. Print all items from a specific vendor"+"\n"+
                    "5. Exit"+"\n"+
                    "Enter your slection > ";

            value =JOptionPane.showInputDialog(null,
                    output,"Input Data",JOptionPane.QUESTION_MESSAGE);
            selection=Integer.parseInt(value);
            while(selection!=5)
            {
                if(selection ==1)
                {
                    value =JOptionPane.showInputDialog(null,
                            "Enter the item number to search for","Input Data",JOptionPane.QUESTION_MESSAGE);
                    search_num=Integer.parseInt(value);
                    for(i=0;i<=count;++i)
                    {
                        if(search_num==inumber[i])
                        {
                            System.out.println("Item Found:"+"\n"+
                                    inumber[i]+"  "+iname[i]+"  "+vname[i]+"  "+
                                    vprice[i]+"  "+rprice[i]+"  "+stock[i]);
                        }
                    }
                }
                else
                if(selection ==2)
                {
                    System.out.println("    ALL INVENTORY ");
                    System.out.println("    =============");
                    for(i=0;i<=count;++i)
                    {
                        System.out.println(inumber[i]+"  "+iname[i]+"  "+vname[i]+"  "+
                                vprice[i]+"  "+rprice[i]+"  "+stock[i]);
                    }
                }
                else
                if(selection ==3)
                {
                    System.out.println("       INVENTORY VALUE");
                    System.out.println("       ===============");
                    double line_amount;
                    double total=0;
                    for(i=0;i<=count;++i)
                    {
                        line_amount=rprice[i]*stock[i];
                        System.out.println(inumber[i]+"  "+iname[i]+" value= "+"  "+line_amount);
                        total=total+line_amount;
                    }
                    System.out.println("TOTAL VALUE OF INVENTORY = "+total);
                }
                else
                if(selection ==4)
                {
                    String search_company =JOptionPane.showInputDialog(null,
                            "Enter the Lumber Company to search for","Input Data",JOptionPane.QUESTION_MESSAGE);
                    for(i=0;i<=count;++i)
                    {
                        if(search_company.equals(vname[i]))
                        {
                            System.out.println(inumber[i]+"  "+iname[i]);
                        }
                    }
                }

                output="ACME LUMBER MENU"+"\n"+
                        "=================="+"\n"+
                        "1. Search for an item"+"\n"+
                        "2. Print all items"+"\n"+
                        "3. Print the value of Inventory"+"\n"+
                        "4. Print all items from a specific vendor"+"\n"+
                        "5. Exit"+"\n"+
                        "Enter your slection > ";

                value =JOptionPane.showInputDialog(null,
                        output,"Input Data",JOptionPane.QUESTION_MESSAGE);
                selection=Integer.parseInt(value);
            }//end of while loop
        }//end menu
        //******************************************************************************


        //******************************************************************************
        public static int add(int count,int[]inumber,String[]iname,
                              String[]vname,double[]vprice,double[]rprice,int[]stock)
        {
            String value;
            count=count+1;
            value =JOptionPane.showInputDialog(null,
                    "Enter the item number","Input Data",JOptionPane.QUESTION_MESSAGE);
            inumber[count]=Integer.parseInt(value);
            iname[count] =JOptionPane.showInputDialog(null,
                    "Enter the item name","Input Data",JOptionPane.QUESTION_MESSAGE);
            vname[count] =JOptionPane.showInputDialog(null,
                    "Enter vendor name","Input Data",JOptionPane.QUESTION_MESSAGE);
            value =JOptionPane.showInputDialog(null,
                    "Enter vendor price","Input Data",JOptionPane.QUESTION_MESSAGE);
            vprice[count]=Double.parseDouble(value);
            value =JOptionPane.showInputDialog(null,
                    "Enter retail price","Input Data",JOptionPane.QUESTION_MESSAGE);
            rprice[count]=Double.parseDouble(value);
            value =JOptionPane.showInputDialog(null,
                    "Enter the stock","Input Data",JOptionPane.QUESTION_MESSAGE);
            stock[count]=Integer.parseInt(value);
            return count;
        }//end add

    }

    public static void main(String[] args)
    {
        lumber_data ld = new lumber_data();
        lumber_manipulate lm = new lumber_manipulate();


        int selection = lm.menu();
        while (selection !=3)
        {
            if(selection ==1)
                ld.count=lm.add(  ld.count,  ld.inumber,  ld.iname,  ld.vname,  ld.vprice,  ld.rprice,  ld.stock);
            else
            if (selection ==2)
                lm.report(  ld.count,  ld.inumber,  ld.iname,  ld.vname,  ld.vprice,  ld.rprice,  ld.stock);
            selection = lm.menu();
        }//end of while loop
        ld.end_program(  ld.count,  ld.inumber,  ld.iname,  ld.vname,  ld.vprice,  ld.rprice,  ld.stock);
    }//end of main method




//******************************************************************************

}//end of main class